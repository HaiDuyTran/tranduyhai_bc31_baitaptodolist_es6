const BASE_URL = "https://62cd305aa43bf780085327e1.mockapi.io/toDoList";

export let viecLamServices = {
  layDanhSachViecLam: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  xoaViecLam: (idViecLam) => {
    return axios({
      url: `${BASE_URL}/${idViecLam}`,
      method: "DELETE",
    });
  },
  themViecLam: (viecLam) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: viecLam,
    });
  },
  layThongTinChiTietViecLam: (idViecLam) => {
    return axios({
      url: `${BASE_URL}/${idViecLam}`,
      method: "GET",
    });
  },
};
