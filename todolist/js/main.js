import { viecLamControllers } from "./controllers/viecLamControllers.js";
import { viecLamServices } from "./Services/viecLamServices.js";

let toDoList = [];
let finishedList = [];
let DomID = "todo";
let renderTable = (list) => {
  let contentHTML = "";
  for (let i = 0; i < list.length; i++) {
    let toDoItem = list[i];
    let contentTr = `<li>
        <div>${toDoItem.name}</div>
        <div class="flex-row">
          <button>
            <i class="fa fa-trash-alt" onclick="xoaViecLam(${toDoItem.id})"></i>
          </button>
          <button >
            <i class="fa fa-check-circle"  onclick="checkViecLam(${toDoItem.id})"></i>
          </button>
        </div>
      </li>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById(`${DomID}`).innerHTML = contentHTML;
};
let renderTable2 = (list) => {
  let contentHTML = "";
  for (let i = 0; i < list.length; i++) {
    let toDoItem = list[i];
    let contentTr = `<li>
        <div>${toDoItem.name}</div>
        <div class="flex-row">
          <button>
            <i class="fa fa-trash-alt"></i>
          </button>
          <button>
            <i class="fa fa-check-circle" class="btn btn-primary" id="checkButton${toDoItem.id}" ></i>
          </button>
        </div>
      </li>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById(`${DomID}`).innerHTML = contentHTML;
};
let renderDanhSachViecLam = (toDoList, DomID) => {
  viecLamServices
    .layDanhSachViecLam()
    .then((res) => {
      toDoList = res.data;
      console.log(toDoList);
      toDoList.sort((a, b) => {
        let fa = a.name.toLowerCase(),
          fb = b.name.toLowerCase();

        if (fa < fb) {
          return -1;
        }
        if (fa > fb) {
          return 1;
        }
        return 0;
      });
      renderTable(toDoList, DomID);
    })
    .catch((err) => {});
};
renderDanhSachViecLam(toDoList);
let xoaViecLam = (idViecLam) => {
  viecLamServices
    .xoaViecLam(idViecLam)
    .then((res) => {
      renderDanhSachViecLam(toDoList);
    })
    .catch((err) => {});
};
window.xoaViecLam = xoaViecLam;

let themViecLam = (viecLam) => {
  viecLam = viecLamControllers.layThongTinTuForm();
  viecLamServices
    .themViecLam(viecLam)
    .then((res) => {
      renderDanhSachViecLam(toDoList);
    })
    .catch((err) => {});
};
window.themViecLam = themViecLam;

let checkViecLam = (id) => {
  DomID = "completed";
  viecLamServices
    .layThongTinChiTietViecLam(id)
    .then((res) => {
      toDoList = res.data;
      console.log(toDoList);
      finishedList.push(res.data);
      renderTable2(finishedList, DomID);
    })
    .then((res) => {
      viecLamServices
        .xoaViecLam(id)
        .then((res) => {
          DomID = "todo";
          renderDanhSachViecLam(toDoList, DomID);
        })
        .catch((err) => {});
    })
    .catch((err) => {});
};
window.checkViecLam = checkViecLam;
