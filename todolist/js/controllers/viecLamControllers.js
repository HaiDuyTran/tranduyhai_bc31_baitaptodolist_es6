export let viecLamControllers = {
  layThongTinTuForm: () => {
    let tenViecLam = document.getElementById("newTask").value;
    let viecLam = {
      name: tenViecLam,
    };
    return viecLam;
  },
  timKiemViTri: (list, id) => {
    return list.findIndex((item) => {
      return item === id;
    });
  },
};
